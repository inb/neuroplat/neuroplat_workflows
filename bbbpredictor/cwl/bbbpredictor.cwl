#!/usr/bin/env cwl-runner

cwlVersion: v1.0
class: CommandLineTool
label: Wrapper to run BBBpredictor using singularity
requirements:
  DockerRequirement:
    dockerPull: shub://martinobertoni/BBBpredictor
baseCommand: [python, /opt/run_BBB_predictor.py]
inputs:
  infile:
    type: File
    inputBinding:
      position: 1
  format:
    type:
      - "null"
      -  type: enum
         symbols: [smiles, SMILES, inchi, INCHI, InChI]
         inputBinding:
           position: 2
outputs:
  predictions_outfile:
    type: File
    outputBinding:
      glob: '*_BBBpredictions.tsv'