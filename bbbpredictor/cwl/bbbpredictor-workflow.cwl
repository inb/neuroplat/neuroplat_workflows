cwlVersion: v1.0
class: Workflow

inputs:
  infile: File
  format:
    type:
      - "null"
      -  type: enum
         symbols: [smiles, SMILES, inchi, INCHI, InChI]

outputs:
  predictions_file:
    type: File
    outputSource: bbbpredictor/predictions_outfile

steps:
  bbbpredictor:
    run: ./bbbpredictor.cwl
    in:
      infile: infile
      format: format
    out: [predictions_outfile]